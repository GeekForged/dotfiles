# Dylan Weremeichik's Dot Files
## Setup
To get setup, simply 
```bash
git clone --bare git@gitlab.com:GeekForged/dotfiles.git $HOME/.dots
cd ~/
git --git-dir=$HOME/.dots/ --work-tree=$HOME checkout
```
## Dependencies
Note that this depends on the fish shell being installed and in use.
