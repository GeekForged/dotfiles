function openw --description 'Open a website in your browser as https'
    open "https://$argv[1]"
end
