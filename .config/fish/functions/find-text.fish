function find-text --description 'Searches the files in and below a directory for a text string, returns snippets with line numbers.'
    fgrep -irn --colour $argv[1]
end
