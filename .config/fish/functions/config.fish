function config --description 'Git wrapper for home directory dot files'
    if test "$argv[1]" = 'mask'
        config config --local status.showUntrackedFiles no
    else if test "$argv[1]" = 'config-init'
        git init --bare $HOME/.dots
        config mask
    else if test "$argv[1]" = 'config-clone'
        git clone --bare $argv[2] $HOME/.dots
    else 
        /usr/bin/git --git-dir=$HOME/.dots/ --work-tree=$HOME $argv
    end
end
