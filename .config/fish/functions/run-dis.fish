function run-dis --description 'Run an application in the current directory and disown it.'
    $argv[1] . &; disown
end
